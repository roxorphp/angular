import { TestBed, async } from "@angular/core/testing";
import { TestComponent } from "./test.component";

describe('tester composant', () => {

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        declarations: [TestComponent]
      }).compileComponents()  // pour que la transformation des templateUrl
    }))

    it('tester le titre', () => {
       const fixture = TestBed.createComponent(TestComponent)
       fixture.detectChanges()
       // const component = fixture.componentInstance 
       // const titre = component.title 
       const tpl = fixture.nativeElement 
       const el = tpl.querySelector('h1')
       console.log(el)
       expect(el.textContent).toEqual('My app')
    })
})