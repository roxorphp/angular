import { Component } from '@angular/core'
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { forbiddenLetter } from '../shared/validators/forbidden.validator';
import { UsersService } from '../core/users.service';  // Bien penser à les mettre dans provider de app.module.ts
import { AuthService } from '../core/auth.service';  
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({  // dans le pipe currency on ne peut charger fr que si localeFr est importé dans app.module.ts
    templateUrl: 'login.component.html',
    selector: 'app-login',
    styles: [   // style css propre au composant (styles)
        `.red { color: red} 
         .green { color: green }
    `]
})
export class LoginComponent {
    myForm: FormGroup
    propEmail: FormControl
    propPassword: FormControl
    send: boolean = false
    
    // Injection du service FormBuilder
    constructor(
        private builder:FormBuilder, 
        private usersService:UsersService,
        private auth:AuthService,
        private router:Router
    ){}

    ngOnInit(){

        this.redirectIfLogged()

        this.propEmail = new FormControl('valeur default email', 
        [  // liste des valdiateurs synchrone
            Validators.required,
            Validators.minLength(3),
            forbiddenLetter('x')  // Cette focntion
        ],
        [  // liste des validateur asynchrone (promesse ou observable)
            this.usersService.checkEmail.bind(this.usersService)  // si on ne met pas bind this dans checkEmail vaudra undefined
        ]
        )
        this.propEmail.setValue('new valeu')

        this.propEmail
            .valueChanges 
            .pipe(
              debounceTime(1000),  // va metre une temps 1s 
              distinctUntilChanged()  // deect un chamnheùent avec la derniere valeur
            )
            .subscribe((str:string) => console.log(str))

      
        this.propPassword = new FormControl()  // Utiliser zxcvbn poour la complexité du mot de pass
        // liaiso ndes champs de form avec les propriété de la class
        this.myForm = this.builder.group({
            email: this.propEmail,
            password: this.propPassword
        })

        // this.myForm.value = { email: 'lol'}
    }

    login (form) { 
        this.send = true
        this.auth.login(this.myForm.value).then(s => this.redirectIfLogged())
    }

    redirectIfLogged(){
        if(this.auth.token){
            this.router.navigate(['/'])
        }
    }
}