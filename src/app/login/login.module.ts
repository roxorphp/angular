import { NgModule } from '@angular/core'
import { LoginComponent } from './login.component'
import { CommonModule } from  '@angular/common';  // pour les ngIf etc (directives)
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
   declarations: [LoginComponent],  // liste des composants se trouvant dans le modules
   imports: [
      CommonModule,
      ReactiveFormsModule  // /!\ Ne pas utilisé en meme temps que FormModule (avec ngModel)
   ]  // liste des sous-modules
})
export class LoginModule { }