import { Component } from '@angular/core';
import { SocketService } from './core/socket.service';

@Component({
  selector: 'app-root',  // <app-root> dans app;component.html
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
  constructor(socket: SocketService){}  // ici pas de private car pas besoin d'une propriété . C'ets jsuet poru initier l'instance du service
  title = 'myproject';
}