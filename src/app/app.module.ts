import { NgModule, Injector } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { LoginModule } from './login/login.module'
import { AppComponent } from './app.component'
import localeFr from '@angular/common/locales/fr'
import { registerLocaleData } from '@angular/common'
import { appRouter } from './app.router'
import { LayoutModule } from './layout/layout.module'
import { AppService } from './core/app.service'
import { UsersService } from './core/users.service'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { AuthService } from './core/auth.service';
import { layoutRouter } from './layout/layout.router';
import { AuthGuard } from './core/auth.guard';
import { InterceptorService } from './core/interceptor.service';
import { SocketService } from './core/socket.service';
import { DatabaseService } from './core/db.service';
import { TestComponent } from './test/test.component';

registerLocaleData(localeFr, 'fr')

/*
// Classe qui va etre appelé avant tout
const injector = Injector.create({
    providers: [
        {
            provide:  InterceptorService,
            // useValue: 'fake',
            useClass: InterceptorService,
            deps: [],
            multi: true
        },
        {
            provide:  InterceptorService,
            // useValue: 'fake 2',
            useClass: InterceptorService,
            deps: [],
            multi: true
        }       
    ]
})

const instance = injector.get(InterceptorService)
console.log(instance)
*/
@NgModule({
   declarations: [AppComponent, TestComponent],  // liste des composants se trouvant dans le modules
   imports: [
       BrowserModule, 
       LayoutModule,
       LoginModule,
       HttpClientModule,
       appRouter,
       layoutRouter
   ],  // liste des sous-modules
   bootstrap: [AppComponent],  // composant de démarrage (point de démarrage)
   providers: [
       /** { provide: AppService, useClass: AppService }, */ 
       AppService, 
       UsersService, 
       AuthService, 
       AuthGuard,
       {   // On va dire angular d'utiliser notre intercep^teur au lieu du sien
           provide: HTTP_INTERCEPTORS, // tableau d'intercepteur angulaar fera une boucle dessus
           useClass: InterceptorService, // Angular va finalement utiliser notre class au lieu de HTTP_INTERCEPTORS
           multi: true  // necesaire car HTTP_INTERCEPTORS est un tableau
       },
       SocketService,
       DatabaseService
    ] // liste des services qui vont etre isntancié (une fois) et propager dans tous les modules / composants
})
export class AppModule { }