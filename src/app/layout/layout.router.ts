import { Routes, RouterModule } from "@angular/router";
import { LayoutComponent } from "./layout.component";
import { HomeComponent } from "./home/home.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { AuthGuard } from "../core/auth.guard";

const routes:Routes = [
    {
        path: '',
        component: LayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                component: HomeComponent
            },
            {
                path: 'user/:id',
                component: UserProfileComponent
            }
        ]
    }
]

export const layoutRouter = RouterModule.forChild(routes)  // /!\ ne pas utiliser forRoot sinon ca av ecraser app.router.ts