import { Component, OnInit } from '@angular/core'
import { UserInterface } from '../../shared/interfaces/user.interface'
import { UsersService } from 'src/app/core/users.service'
import { SocketService } from 'src/app/core/socket.service';

@Component({
    templateUrl: 'users.component.html',
    selector: 'app-home'
})
export class HomeComponent implements OnInit {
    nbSelected: number = 1
    extensions: string[] = ['tv', 'biz', 'io', 'me']
    lang: string
    users: UserInterface[] = []
    search: string

    constructor (private usersService: UsersService, private socket:SocketService){}

    async ngOnInit () {  // Equivalent methode oldNgOnInit
        this.usersService.searchObserver.subscribe(data => {
           this.search = data
        })

        this.socket.userObserver.subscribe((data) => {
            this.deleteUser(data.id)
        })

        this.users = await this.usersService.fetch()
    }

    createUser(){
        this.usersService.create({ name: 'jim'}).then((u:UserInterface) => {
           // this.users.push(u)  // ajoute à la fin
           // this.users = [u, ...this.users] // ajoute au debut
           this.users.splice(1, 0, u)  // a joute en avant dernier
        })
    }

    deleteUser(id: number){
      console.log(id)  // id vaut $event 
      const i = this.users.findIndex(user => user.id == id)
      this.users.splice(i,1)
    }
}