import { Component, Input } from '@angular/core'
import { UsersService } from 'src/app/core/users.service';
import { UserInterface } from 'src/app/shared/interfaces/user.interface';

@Component({  
    // Si on met u nattribut entre [] > entrée on peut lui assigné une valeur via prop dobjet, si ()  > sortie il peut aussi modifier la prop d'e l'objet
    // Ngif est une directive structurelle préfixée de *
    templateUrl: 'user.component.html',
    selector: 'app-user'
})
export class UserComponent {
    // Input et Output sont des décorateurs
    @Input() user: UserInterface // La valeur sera d&finit par le compsoant parent (nAVbAR) 
    
    constructor(private usersService:UsersService){}

    delete() {
        this.usersService.remove(this.user.id)
        // this.onDelete.emit()
    }
}