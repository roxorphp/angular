import { Pipe, PipeTransform } from '@angular/core'
@Pipe({ 
    name: 'search'
})
export class SearchPipe implements PipeTransform {
    transform(users: any[], keyword: string): any[] {
        return users.filter((name)=> name.toLowerCase().startsWith(keyword.toLowerCase()))
    }
}