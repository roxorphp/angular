import { NgModule } from  '@angular/core'
import { HomeComponent } from './home.component'
import { UserComponent } from './user/user.component'
import { CommonModule } from  '@angular/common' // pour les ngIf etc (directives)
import { SharedModule } from '../../shared/shared.module'
import { UserFilterPipe } from './pipes/userfilter.pipe'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { SearchPipe } from './pipes/search.pipe';


@NgModule({
    imports: [CommonModule, SharedModule, FormsModule, RouterModule],  // /!\ Uniquement les modules
    declarations: [HomeComponent, UserComponent, UserFilterPipe, SearchPipe],
    exports: [HomeComponent]  
})
export class HomeModule {

}