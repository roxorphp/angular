import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/core/users.service';
import { UserInterface } from 'src/app/shared/interfaces/user.interface';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  user: UserInterface | object | any = {}  // pour éviter les erreurs

  editForm: FormGroup
  // Champs du formulaire 
  name: FormControl
  email: FormControl

  constructor(
    private route: ActivatedRoute,
    private userService: UsersService,
    private builder: FormBuilder
  ) {}

  get description(){
     return this.user.name + '888'
  }

  // Selon la valeur du pâram id de l'url on chage l'utilsiateur
  ngOnInit() {

     this.editForm = this.builder.group({  // doit refléter l'objet user
      email: '',  // bien si pas d valdiateur sp^écific
      name: ''
      // name: ['', ...]  // revient à faire le new FormControl
     })

     // on écoute le changement du paramètre id
     this.route.params.subscribe((paramsUrl => {
        this.userService.find(paramsUrl.id).subscribe(u => {
          console.log(u)
          this.user = u
          this.editForm.patchValue(this.user)
          console.log(this.user)
        })
     }))
  }

  edit(){
      const user = this.editForm.value
      user.id = this.user.id
      console.log(user)
      this.userService.edit(user).then(u => {
        alert('Edition OK')
        this.user = u
        console.log(u)
      })
  }

  _edit(){
      this.userService.edit(this.user).then(u => {
        alert('Edition OK')
        console.log(u)
      })
  }

}
