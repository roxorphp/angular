// créer via ng generate module layout/user-profile
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserProfileComponent } from './user-profile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [UserProfileComponent],
  imports: [
    CommonModule, 
    FormsModule,
    ReactiveFormsModule
  ]
})
export class UserProfileModule { }
