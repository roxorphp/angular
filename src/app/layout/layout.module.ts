import { NgModule } from '@angular/core';
import { HomeModule } from './home/home.module';
import { NavBarModule } from './navbar/navbar.module';
import { LayoutComponent } from '../layout/layout.component'
import { UserProfileModule } from './user-profile/user-profile.module';
import { layoutRouter } from './layout.router';

@NgModule({
  imports: [
    HomeModule, 
    NavBarModule, 
    UserProfileModule,
    layoutRouter  // necessaire poru utiliser la directive router-outlet
  ],
  declarations: [LayoutComponent],  // liste des composants se trouvant dans le modules
})
export class LayoutModule { }