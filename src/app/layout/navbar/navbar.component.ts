import { Component, OnInit } from '@angular/core'
import { AppService } from 'src/app/core/app.service'
import { AuthService } from 'src/app/core/auth.service';
import { Router } from '@angular/router';

@Component({  // dans le pipe currency on ne peut charger fr que si localeFr est importé dans app.module.ts
    templateUrl: 'navbar.component.html',
    selector: 'app-navbar'
})
export class NavBarComponent implements OnInit {
    title: string = 'Yo'
    name: string = 'ti'
    price: number = 15
    now: Date = new Date()
    // appService: AppService
    
    // on inject le service AppService
    constructor(private appService: AppService, private auth:AuthService, private router:Router){  // Va créer une propriété appService avec l'instance
        // this.appService = appService  // inutile
    }
    
    // méthode appelé par angular quand le compsant est inititilisé
    ngOnInit() {
        console.log(this.appService.getTitle())
    }

    listenSearch(str: string) {
        console.log(str)  // str vaut la valeur de la propriété username de SearchComponent passé via la méthode emit
    }

    logout () {
        this.auth.logout()
        this.router.navigate(['/login'])
    }
}