import { NgModule } from  '@angular/core'
import { NavBarComponent } from './navbar.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms' // pour la directive NgModel
import { SearchComponent } from './search/search.component';
import { CommonModule } from  '@angular/common';  // pour les ngIf etc (directives)
import { SharedModule } from '../../shared/shared.module';
import { appRouter } from './../../app.router'  // permet d'utiliser la dirctive routerLink dans le btn de login
@NgModule({
    imports: [FormsModule, CommonModule, SharedModule, appRouter, ReactiveFormsModule],
    declarations: [NavBarComponent, SearchComponent],  // composants privé à navbar module
    exports: [NavBarComponent]  // les aiutres composant peuvent utilise navbar
})
export class NavBarModule { }