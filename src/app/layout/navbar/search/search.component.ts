import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core'
import { FormControl } from '@angular/forms';
import { UsersService } from 'src/app/core/users.service';

@Component({  
    // Si on met u nattribut entre [] > entrée on peut lui assigné une valeur via prop dobjet, si ()  > sortie il peut aussi modifier la prop d'e l'objet
    // Ngif est une directive structurelle préfixée de *
    templateUrl: 'search.component.html',
    selector: 'app-search'
})
export class SearchComponent implements OnInit{

    // Input et Output sont des décorateurs
    @Input() username: string = '' // La valeur sera d&finit par le compsoant parent (nAVbAR) 
    @Output() onSearch: EventEmitter<string> = new EventEmitter()
    title: string = 'lol'
    names: Array<string> = ['titi', 'tata', 'toto']
    propSearch: FormControl = new FormControl()
    
    constructor(private userService:UsersService){}

    ngOnInit(): void {
        this.propSearch.valueChanges.subscribe((str: string) => {
            this.userService.searchObserver.next(str)
        })
    }

    searchSubmit(ev: Event, test) {
       /*
       console.log(ev)
       alert(test)
       */
       this.onSearch.emit(this.username)
       ev.preventDefault()
    }
}