import { Component } from '@angular/core';

@Component({  // router-outlet va charger dynamqiement u ncompsoant selon la pvaleur de path
  template: `
  <app-navbar></app-navbar>
  <router-outlet></router-outlet>
  `,
  selector: 'app-layout'  // <app-root> dans app;component.html
})
export class LayoutComponent {

}