import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";

@Injectable(/*{
    providedIn: 'root'  // depuis A6 permet d'evotrer de la definir oit meme dans  prodivers de app.module.ts
}*/)
export class AuthGuard implements CanActivate{
    constructor(
        private auth:AuthService,
        private router: Router  // pour rediriger
    ){}
     // method que va appeler angular:
    canActivate(): boolean
    {
        if(this.auth.token){
            return true
        }

        this.router.navigate(['/login'])
        return false  // utile que pour la signature de la methode
    }
}