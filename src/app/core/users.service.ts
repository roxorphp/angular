import { UserInterface } from '../shared/interfaces/user.interface'
import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { map } from 'rxjs/operators'
import { SocketService } from './socket.service';
import { BehaviorSubject } from 'rxjs';
import { DatabaseService } from './db.service';
import uuid from 'uuid/v4'

@Injectable()  // permet de résoudre le bug de l'injectio nde dépendance HttpClient et d'autoriser l'injectio nde dépendance dans le service
export class UsersService {
    url: string = 'https://jsonplaceholder.typicode.com/users'
    searchObserver: BehaviorSubject<string> = new BehaviorSubject('')
    constructor(
        private http:HttpClient, 
        private socket:SocketService,
        private db:DatabaseService
        ) {}
    
    // DELETE
    remove(id: number) {
        return this.http.delete(this.url+'/'+ id)
               .toPromise()
               .then(() => {
                   this.socket.emit('delete.user', { id })  // on emet un event au serveur node
               })
    }

    // POST
    async create(data: Object): Promise<object>
    {
        data['_id'] = uuid()  // uuid en théroie requis par pushdb
        const user = await this.http
          .post(this.url, data)
          .toPromise()
        
        await this.db.create(data);

        return user
    }

    // GET
    fetch(): Promise<any>
    {  // equivalent méthode oldFetch

        if(!navigator.onLine){  // Si on est offline on tape dans la bdd local pour récup la liste des user
            alert('offline')
            return this.db.fetch().then(({rows}) => { 
                console.log(rows) 
                return rows.map(row => row.doc)
            })
        }

        return this.http.get<UserInterface[]>(this.url).toPromise() 
    }

    find(id: number)
    {
        return this.http.get(this.url+'/'+id)
    }

    async edit(data){
        return await this.http.patch(this.url+'/'+data.id, data).toPromise() 
    }

    // vérifie si l'email est deja pris (en mode observable)
    checkEmail(input: FormControl){
        return this.http.get(this.url+'/1')
         .pipe(
             map((user:any) => user.email == input.value ? { checkEmail: true } : null)
         )
    }

    // vérifie si l'email est deja pris (en mode promise)
    _checkEmail(input: FormControl) {
        return new Promise((resolve) => {
            this.http
                .get(this.url+'/1')
                .toPromise()
                .then((user:UserInterface) => {
                   if(user.email == input.value){
                       resolve({ checkEmail: true })
                   }
                   else {
                       resolve(null)
                   }
                })
        })
    }

    oldFetch2(){
        // return this.http.get(this.url)  
        return new Promise((resolve, reject) => {  // La promesse ne sera résolu qu'une fois, seul la 1ere entrée du flux sera retorunée
            this.http.get(this.url)
              .subscribe(resolve, reject)
              /*
              .subscribe((users: UserInterface[]) => {  // subscruibe s'applique sur un observer
                  resolve(users)
              }, (err) => reject(err))
              */
        })
    }
}