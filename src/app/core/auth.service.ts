import { HttpClient } from "@angular/common/http";
import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
    constructor(private http:HttpClient){}

    // Login POST
    async login(identifier: Object)
    {
        try {
            const {token} = await this.http.post<any>('https://reqres.in/api/login', identifier).toPromise()
            this.token = token  // this.token(token)
            alert('OK')
        }
        catch(e){  // se déclenchera si une erreur 400 ou autre est rencontrée
            alert('echec')
            console.log(e)
        }
    }

    logout () {
        localStorage.removeItem('angular-token')
    }

    // accesser / getter magic 
    get token () {
        return localStorage.getItem('angular-token')
    }

    set token (token) {
        localStorage.setItem('angular-token', token)
    }

    get isLogged () {
        return this.token != null
    } 
}