import { HttpInterceptor, HttpRequest, HttpHandler, HttpHeaders } from "@angular/common/http";
import { AuthService } from "./auth.service";
import { Injectable } from "@angular/core";
// Cette classe (service) va intercepter les requete et mettre les bons headers
@Injectable()
export class InterceptorService implements HttpInterceptor {

    constructor(private auth:AuthService){}

    intercept(req: HttpRequest<any>, next: HttpHandler){

       if(!this.auth.token){
           return next.handle(req)
       }
    
       const headers = new HttpHeaders().set('Authorization', this.auth.token)  // Bearer token
       const newReq = req.clone({ headers } /** { headers : headers } */)  // La requete initial ne peut ete cloné donc on la cone en la modifiant sa prop header dans la foulé 
       return next.handle(newReq)
    }
}