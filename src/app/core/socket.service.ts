import { Injectable } from "@angular/core";
import io from 'socket.io-client'  // installé via npm install  socket.io-client
import { Observable, BehaviorSubject } from "rxjs";


@Injectable()
export class SocketService {
    private socket: any = io('http://localhost:3000')
    // userObserver: Observable<any>
    userObserver:any
    constructor(){

        
        this.userObserver = new BehaviorSubject({})  // subscriber aurto declenché
        // On écoute le flux de sup^presison d'user auquel aon va souscire
        this.socket.on('user.deleted', data => {  // on ecoute l'evenement user.deleted
           this.userObserver.next(data)
        })
        
        /*
        this.userObserver = new Observable((observer) => {
            this.socket.on('user.deleted', data => {  // on ecoute l'evenement user.deleted
                observer.next(data)
            })
        })*/
    }
    emit (event: string, data: object){
        this.socket.emit(event, data)
    }
}