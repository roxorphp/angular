import PouchDb from 'pouchdb-browser'

export class DatabaseService {
    db:  PouchDb = new PouchDb('users')
    create(data: any): Promise<any>
    {
        return this.db.put(data)
    }
    
    fetch(){
        return this.db.allDocs({  // permet de tout avoir au lieu d'un tableau d'identifiants
            include_docs: true
        })
    }
}