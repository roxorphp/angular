import { Pipe, PipeTransform } from '@angular/core'
@Pipe({ 
    name: 'autocomplete'
})
export class AutoCompletePipe implements PipeTransform {
    transform(names: string[], keyword: string): string[] {
        return names.filter((name)=> name.toLowerCase().startsWith(keyword.toLowerCase()))
    }
}