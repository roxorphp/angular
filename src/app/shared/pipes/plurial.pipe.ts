// pemrettra d'utiliser le pipe myProp  | plurial
import { Pipe, PipeTransform } from '@angular/core'
@Pipe({
    name: 'plurial'
})
export class PlurialPipe implements PipeTransform {
    transform(str: string, nb: number): string {
        return str + (nb > 1 ? 's':'')
    }
}