// pemrettra d'utiliser le pipe myProp  | plurial
import { Pipe, PipeTransform } from '@angular/core'
const WORDS: Object = {
    REMOVE : {
        en : 'remove',
        fr : 'supprimer'
    }
}

@Pipe({
    name: 'lang'
})
export class LangPipe implements PipeTransform {

    transform(word: string, lang:string): string {
        return WORDS[word][lang]
    }
}