import { FormControl } from "@angular/forms";

export function forbiddenLetter (letter: string) {
    return function (input: FormControl): object|null { // ce param sera autoùatiquemen inject par angular (oO)
        const regexp = new RegExp(letter, 'i')
        return regexp.test(input.value) ? { letter } : null
    }
}