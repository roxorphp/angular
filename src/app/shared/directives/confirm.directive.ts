import { Directive, HostListener, Input, Output, EventEmitter } from "@angular/core";

@Directive({
    selector: '[confirm]'  // selecteur css (ici toptu element html contenant l'attribut confirm (<el confirm></el>))
})
export class ConfirmDirective { // a mettre dans shared module (declaration + exports)
   
    @Input('confirm') message: string // vaudra ce qui est en affectatio nde l'attribut confirm (confirm="My message")
    @Input() userName: string  // vaut la valeur de l'atrribut userName sur lequel la directive est appliqué
    @Output() onConfirm: EventEmitter<any> = new EventEmitter()

    @HostListener('click')  // la méthode se déclenchera au click
    openDialog(){
        if(confirm(this.message + this.userName))
        {
            this.onConfirm.emit()
        }
    }
}