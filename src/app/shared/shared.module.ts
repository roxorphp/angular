import { NgModule } from '@angular/core'
import { PlurialPipe } from './pipes/plurial.pipe';
import { LangPipe } from './pipes/lang.pipe';
import { AutoCompletePipe } from './pipes/autocomplete.pipe';
import { ConfirmDirective } from './directives/confirm.directive';

const declarations = [PlurialPipe, AutoCompletePipe, LangPipe, ConfirmDirective]  // sera assigné pour les declarations et exports

@NgModule({
    imports: [],
    declarations,  // on déclare tout sauf les modules
    exports: declarations
})
export class SharedModule { }