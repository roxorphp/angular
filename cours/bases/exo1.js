/// 1:

const array = [[{ name: 'ana', age: 18 }, { name: 'ben', age: 19 }], [{ name: 'sam', age: 15 }, { name: 'jim', age: 30 }]]

for (let group of array) {
    for (let user of group) {
        console.log(`${user.name} ${user.age}`)
    }
}

/// 2:

const calculator = {
    exec ({ operator, values }) {
        return eval(values.join(operator))
    }
    // ou
    exec: (obj) => {
        const { operator, values } = obj
        return eval(values.join(operator))
    }
}

const value1 = calculator.exec({
    operator: '+',
    values: [1, 2]
})

const value2 = calculator.exec({
    operator: '-',
    values: [1, 2]
})

console.log(value1, value2); // affiche 3, -1

// exo 3

const add = function (nb1) {
    return function (nb2) {
        return nb1 + nb2;
    }
}

const result = add(1)(2);
console.log(result) // affiche 3

// ou
const add = nb1 => param => nb1 + param;

const result = add(1)(2);
console.log(result) // affiche 3