// pour convertir en js ES5 faire tsc main.js
// ou utiliser parcel.js
// npm install -g parcel-bundler
// parcel index.html
// npm install lodash poru avori des methode bonus genre isArray()

import _ from 'lodash'
import {MyModule, URL_API} from './myModule'  // import la classe MyModule + constante URL_API
const test: string = 'jim'
console.log(test)
const names:Array<string> = ['jim', 'ana']  // tableau de string 
// const names: string[] = ...
if(_.isArray(names)){
    console.log('tableau !')
}

const instance = new MyModule()
instance.render()