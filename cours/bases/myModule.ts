// totue variable définit dans ce fichier ne sera aps dispo ailleurs
// export default class MyModule { }  // permet de donner le nom qu'on veut (il ne peut y en avoir qu'un)
// décorateur 
/*
function template(constructor){
    // constructor fait réfrence a la class MyModule
   console.log(constructor)
   constructor.prototype.template = '<p>test</p>'
}

@template  // on rajoute le décorateur à la classe MyModule
*/

function template(str){  // str vaut ce que va prendre en parametre @template jsute au dessus de la class MyModule
    return function(constructor){  // constructor fait réfrence a la class MyModule
        constructor.prototype.template = str
    }
}

@template(`<p>Test 5</p>`)
export class MyModule {
    //template: string
    constructor(){
        console.log('loaded')
    }
    render(){
        app.innerHTML = this.template
    }
}

export const URL_API = 'toast'