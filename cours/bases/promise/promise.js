// executabme via nopde avec la commande "node promise.js"
// import http from axios // charegment ESM
// import { parseHttpResponse } from 'selenium-webdriver/http';
const http = require('axios')  // chargement commonJS (CJS)

/*
async function foo(){  // on ne peut utilsier resolve et reject
    const users = await http.get('https://jsonplaceholder.typicode.com/users') // va attendre que la requete ajax se finisse
    return users
}

foo().then(r => console.log(r))
*/
/*
const timer = function (data) {
    return new Promise((resolve, reject)=>{
        setTimeout(()=> {
            console.log('fin')
            resolve(data)
        }, 2000)
    })
}
*/

const timer = function () {
    return new Promise((resolve, reject)=>{
        setTimeout(()=> {
            console.log('fin')
            resolve()
        }, 2000)
    })
}

/*
http.get('https://jsonplaceholder.typicode.com/users')  // retourne une promesse
    .then((res) => { return res.data})
    .then(res => timer(res))
    .then(users => console.log(users))
    .catch(e => console.log(e.message))*/
// amélioration
async function run(){  //ici res est utilisable partoru dans la onction
    try {
        const {data} = await http.get('https://jsonplaceholder.typicode.com/posts')  // data vaut la propriété data de l'object renvoyé
        await timer()
        console.log(data)
    }
    catch(err){
        console.log(err)
    }
}

run()
console.log(888)

http.get('https://jsonplaceholder.typicode.com/users')  // retourne une promesse
    .then((res) => { return res.data})
    .then(res => timer(res))
    .then(users => console.log(users))
    .catch(e => console.log(e.message))


new Promise ((resolve, reject) => {
    setTimeout(() => {
        // reject(new Error("Détail de l'erreur"))  // stop l'executio ndu code de la promesse  et va executer le catch
        resolve('Yo')  // déclenchera la méthode then
    }, 2000)})
    .then((str)=> { 
        console.log(str)  // se déclenchera quand la promesse sera résolue et affiche 'YO'
        // On retorune une ^rommetsse pour faire d el'assynchrone
        return new Promise(((resolve, reject) => {
            setTimeout(() => {
                // reject(new Error("Détail de l'erreur"))  // stop l'executio ndu code de la promesse  et va executer le catch
                resolve('Hello')  // déclenchera la méthode then
            }, 3000)}))  // va attendre 3 3 pour retourn hello et le transférer au prochain then
    })
    .then((str2)=> { // se déclenchgera après le précdent then
        console.log(str2)  // se déclenchera quand la promesse sera résolue et affiche Hell oaprès 3 secondes 
    })
    .catch((error)=> {
        console.log(error)
    })