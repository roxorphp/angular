// npm install socket.io 
// npm install express (gestion server node)
// npm install nodemon -g (pour le hot reload )
// npm install  socket.io-client
// nodemon server/main pour lancer le serveur
const app = require('express')() // o ncréer une appliquatio nexpress 
const http = require('http').Server(app)
const io = require('socket.io')(http)  // pour socket ecoute aussi le port 3000

app.get('/lol', (req, res) => {  // la route localhost:3000/lol affichera ok
   res.send('ok')
})

io.on('connection', (socket) => {
    console.log('connecté')
    socket.on('delete.user', (data) => {
        console.log(data)  // data vaut {id: x } 
        // socket.broadcast.emit('user.deleted', data) // seul les autre cliejnt auront l'eventmeent 
        io.emit('user.deleted', data)  // totu le monde y compris nosu meme aurotn l'event
    }) 
})

http.listen(3000)   // ecoute port localhost:3000